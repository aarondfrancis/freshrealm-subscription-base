<?php

namespace freshrealm\subscription\base\controllers;

use freshrealm\subscription\base\models as models;

use common\models\Customer;
use common\models\Product;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\VerbFilter;
use yii\helpers\ArrayHelper;

use backend\components\RbacController;

/**
 * PlanController implements the CRUD actions for Plan model.
 */
class PlanController extends RbacController {
	public $layout = '/column2';
	public $defaultAction = 'index';

	public $sideMenuItems = array();

	public function accessMap(){
		return [
			// namespace
			'subscription',
			[
				// rbac permission name(s), action(s)
				['sync','sync'],
				['list', 'index'],
				['edit', 'update'],
				['delete', 'delete'],
				['create', 'create'],
				['view', ['view','orders','items', 'subscribers', 'orderdays']]
			]
		];
	}


	public function behaviors() {
		$behaviors = array(
			'verbs' => array(
				'class' => VerbFilter::className(),
				'actions' => array(
					'delete' => array('post'),
				),
			),
		);

		return ArrayHelper::merge($behaviors, parent::behaviors());
	}

	public function beforeAction($action){
		// 2 column actions
		$columns = array('items','view', 'subscribers', 'update', 'orderdays', 'orders');
		if(in_array($action->id, $columns)){
			$this->sideMenuItems = [];

			if($this->can('view')) $this->sideMenuItems[] = array(
				'label' => 'Details',
				'url' => array('plan/view', 'id'=>$id = \Yii::$app->request->get('id'))
			);

			if($this->can('view')) $this->sideMenuItems[] = array(
				'label' => 'Orders',
				'url' => array('plan/orders', 'id'=>$id)
			);

			if($this->can('edit')) $this->sideMenuItems[] = array(
				'label' => 'Update',
				'url' => array('plan/update', 'id'=>$id)
			);

			if($this->can('view-items')) $this->sideMenuItems[] = array(
				'label' => 'Items',
				'url' => array('plan/items', 'id'=>$id)
			);

			if($this->can('view')) $this->sideMenuItems[] = array(
				'label' => 'Subscribers',
				'url' => array('plan/subscribers', 'id'=>$id)
			);

			if($this->can('view')) $this->sideMenuItems[] = array(
				'label' => 'Order Days',
				'url' => array('plan/orderdays', 'id'=>$id)
			);
		}
		return parent::beforeAction($action);
	}

	/**
	 * Lists all Subscription models.
	 * @return mixed
	 */
	public function actionIndex() {
		$this->layout = '/main';
		$searchModel = new models\Subscription;
		$dataProvider = $searchModel->search($_GET);

		return $this->render('index', array(
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		));
	}


	public function actionItems($id){
		$model = $this->findModel($id);

		// For adding a new Product
		$subscriptionProduct = new models\SubscriptionProduct;

		if($this->can('edit')){
			// See if we're adding a new Product to this Subscription
			// Do *not* call save(), use the Subscription->add() method
			if($subscriptionProduct->load($_POST)){
				\Yii::$app->session->set('active-tab', 'items');
				if($subscriptionProduct->validate()){
					if($model->add($subscriptionProduct->product_id, $subscriptionProduct->quantity)){
						\Yii::$app->session->setFlash('success', "Successfully added
							{$subscriptionProduct->quantity} \"{$subscriptionProduct->product->fresh_name}\".");
						return $this->refresh();

					}else{
						\Yii::$app->session->setFlash('error', 'Something went wrong.');
					}
				}
			}

			// See if we're removing a Product
			if(isset($_POST['removeProduct'])){
				\Yii::$app->session->set('active-tab', 'items');
				if($model->removeAll($_POST['removeProduct'])){
					$product = Product::find($_POST['removeProduct']);
					$name = $product ? " \"$product->fresh_name\"" : "";
					\Yii::$app->session->setFlash('success', "Successfully removed all{$name}.");
					return $this->refresh();
				}else{
					\Yii::$app->session->setFlash('error', 'Something went wrong.');
				}
			}
		}

		return $this->render('items', array(
			'model' => $model,
			'subscriptionProduct'=>$subscriptionProduct
		));
	}

	public function actionSubscribers($id){
		$model = $this->findModel($id);

		// For adding a new Subscriber
		$customerSubscription = new models\CustomerSubscription;

		if($this->can('edit')){
			// See if we're adding a subscriber
			if($customerSubscription->load($_POST)){
				if($model->addSubscriber($customerSubscription->customer_id)){
					\Yii::$app->session->setFlash('success', "Successfully subscribed {$customerSubscription->customer->full_name}.");
					return $this->refresh();
				}else{
					\Yii::$app->session->setFlash('error', 'Something went wrong.');
				}
			}

			// See if we're removing a subscriber
			if(isset($_POST['removeCustomer'])){
				if($model->removeSubscriber($_POST['removeCustomer'])){
					$customer = Customer::find($_POST['removeCustomer']);
					\Yii::$app->session->setFlash('success', "Successfully unsubscribed {$customer->full_name}.");
					return $this->refresh();
				}else{
					\Yii::$app->session->setFlash('error', 'Something went wrong.');
				}
			}
		}


		return $this->render('subscribers', array(
			'customerSubscription'=>$customerSubscription,
			'model' => $model
		));
	}

	public function actionUpdate($id){
		$model = $this->findModel($id);

		// See if we're updating the model
		if($model->load($_POST) && $model->save()) {
			\Yii::$app->session->setFlash('success', 'Subscription successfully updated.');
			return $this->refresh();
		}

		return $this->render('update', array(
			'model' => $model
		));
	}


	public function actionOrderdays($id){
		$model = $this->findModel($id);

		$newSubscriptionDay = new models\SubscriptionDay;

		if($this->can('edit')){
			if($newSubscriptionDay->load($_POST)) {
				$added = $model->addSubscriptionDay(
					$newSubscriptionDay->deliver_day,
					$newSubscriptionDay->order_cutoff_day,
					$newSubscriptionDay->order_cutoff_time
				);
				if($added){
					\Yii::$app->session->setFlash('success', 'Order day saved.');
					return $this->refresh();
				}
			}

			if(isset($_POST['removeOrderDay'])){
				$id = $_POST['removeOrderDay'];
				$newSubscriptionDay = models\SubscriptionDay::find($id);
				$newSubscriptionDay->delete();
				\Yii::$app->session->setFlash('success', 'Order day removed.');
				return $this->refresh();
			}
		}


		return $this->render('orderdays', array(
			'model' => $model,
			'newSubscriptionDay' => $newSubscriptionDay
		));
	}

	public function actionOrders($id){
		$model = $this->findModel($id);

		return $this->render('orders', array(
			'model' => $model
		));
	}



	/**
	 * Displays a single Subscription model.
	 * @param string $id
	 * @return mixed
	 */
	public function actionView($id) {
		$model = $this->findModel($id);

		return $this->render('view', array(
			'model' => $model
		));
	}

	/**
	 * Creates a new Subscription model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new models\Subscription;

		if ($model->load($_POST) && $model->save()) {
			return $this->redirect(array('view', 'id' => $model->id));
		} else {
			return $this->render('create', array(
				'model' => $model
			));
		}
	}

	/**
	 * Deletes an existing Subscription model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param string $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(array('index'));
	}

	/**
	 * Finds the Subscription model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return Subscription the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = models\Subscription::find($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}