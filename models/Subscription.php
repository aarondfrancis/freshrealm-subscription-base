<?php

namespace freshrealm\subscription\base\models;

use freshrealm\subscription\base\models as models;

use common\models\Basket;
use common\helpers\Time;

use yii\data\ActiveDataProvider;

class Subscription extends Basket {

	/**
	 *	@var string name of the create attribute in the DB for this model
	 */
	protected $createAttribute = 'created';

	/**
	 *	@var string name of the update attribute in the DB for this model
	 */
	protected $updateAttribute = 'modified';

	public function init(){
		$this->on('afterProductAddedToSubscription', array($this, 'calculateCost'));
		$this->on('afterProductRemovedFromSubscription', array($this, 'calculateCost'));
		$this->on('afterSubscriptionCleared', array($this, 'calculateCost'));

		return parent::init();
	}

	public static function tableName() {
		return 'subscription';
	}

	/**
	 * See Basket->collectionClass();
	 */
	public function collectionClass(){
		return 'freshrealm\subscription\base\models\SubscriptionProduct';
	}

	public function rules() {
		return array(
			array('name','required'),
			array(['cost','price'],'number'),
			array(['public','featured'], 'boolean'),
			array('customer_id', 'integer'),
			array('name', 'string', 'max' => 255)
		);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'public' => 'Public',
			'featured' => 'Featured',
			'customer_id' => 'Owner',
		);
	}

	public function beforeValidate(){
		if($this->public && $this->customer_id){
			$this->addError('public', 'If you are going to assign this subscription to an
				individual, you cannont make it pulic also.');
		}
		return parent::beforeValidate();
	}

	/**
	 * Calculates the raw cost of the items in the cart. Used as a
	 * hint for pricing this subscription
	 * @return null
	 */
	public function calculateCost(){
		$cost = 0;
		foreach ($this->items as $item) {
			$cost += (float)$item->product->fresh_avgFreshrealmCost * $item->quantity;
		}
		$cost = round($cost, 2);
		$this->cost = $cost;
		$this->save();
	}

	/**
	 * Removes a subscriber from this subscription, also clears the
	 * ledger so that no upcoming orders are processed.
	 * @param $customer_id integer
	 * @return boolean
	 */
	public function removeSubscriber($customer_id){
		$model = CustomerSubscription::find([
			'customer_id'=>$customer_id,
			'subscription_id'=>$this->id
		]);
		if($model){
			$this->clearLedger($customer_id);
			return $model->delete() > 0;
		}else{
			return true;
		}
	}


	/**
	 * Adds a subscriber to this subscription, and enters all the
	 * future order dates into the ledger.
	 * @param $customer_id integer
	 * @return boolean
	 */
	public function addSubscriber($customer_id){
		$model = new CustomerSubscription();
		$model->attributes = [
			'customer_id'=>$customer_id,
			'subscription_id'=>$this->id
		];

		$success = $model->save();

		if($success) $this->processLedgerForCustomer($customer_id);

		return $success;
	}


	/**
	 * Add an order day for this subscription. Also creates ledgers for
	 * this subscription day.
	 * @param int $deliver_day. Day of the week on which to deliver
	 * @param int $order_day. Day of the week on which to order
	 * @param int $order_time. Time by which the order must be placed.
	 * @return boolean success
	 */
	public function addSubscriptionDay($deliver_day, $order_day, $order_time){
		$sd = new SubscriptionDay;
		$sd->subscription_id = $this->id;
		$sd->deliver_day = $deliver_day;
		$sd->order_cutoff_day = $order_day;
		$sd->order_cutoff_time = $order_time;

		$success = $sd->save();
		if($success){
			$this->processLedgerForSubscriptionDay($sd->id);
		}else{
			$this->addError('addSubscriptionDay', $sd->errors);
		}

		return $success;
	}

	/**
	 * Remove an order day from this subscription. Also clears and recreates
	 * the ledger for all customers
	 */
	public function removeSubscriptionDay($subscription_day_id){
		$sd = SubscriptionDay::find($subscription_day_id);
		if($sd){
			$this->clearLedgerForAllCustomers();
			$success = $sd->delete() > 0;
			$this->processLedgerForAllCustomers();
			return $success;
		}else{
			return true;
		}
	}

	/**
	 * Clear the ledger for a customer. Clears future orders.
	 * @param int $customer_id
	 * @return null
	 */
	public function clearLedger($customer_id){
		// Clear all unprocessed (upcoming) orders
		// that this customer may have. Want to start
		// fresh... realm.
		SubscriptionLedger::deleteAll(array(
			'customer_id'=>$customer_id,
			'subscription_id'=>$this->id,
			'order_id'=>null
		));
	}


	/**
	 * Clear all future, unprocessed orders
	 * @return null
	 */
	public function clearLedgerForAllCustomer(){
		SubscriptionLedger::deleteAll(array(
			'subscription_id'=>$this->id,
			'order_id'=>null
		));
	}

	/**
	 * Add an entry into the ledger for a Customer and a SubscriptionDay
	 * @param int $customer_id. Customer id
	 * @param int $subscription_day_id. SubscriptionDay id
	 * @return null
	 */
	public function addLedgerEntry($customer_id, $subscription_day_id){
		$subscriptionDay = SubscriptionDay::find($subscription_day_id);
		if(!$subscriptionDay) return;

		$sl = new SubscriptionLedger;
		$sl->subscription_id = $this->id;
		$sl->customer_id = $customer_id;
		$sl->process_after = $subscriptionDay->nextOrderWindowStart();
		$sl->cutoff_time = $subscriptionDay->nextOrderCutoff()->format(Time::DB_FORMAT);
		$sl->save();
	}


	/**
	 * Adds new ledgers for a given SubscriptionDay. Loops through each
	 * customer and creates the ledger
	 * @param int $subscription_day_id
	 * @return null
	 */
	public function processLedgerForSubscriptionDay($subscription_day_id){
		$this->refreshRelation('customers');
		foreach($this->customers as $customer){
			$this->addLedgerEntry($customer->id, $subscription_day_id);
		}
	}

	/**
	 * Adds new ledgers for a given Customer. Loops through each SubscriptionDay
	 * of this Subscription and adds it for this customer.
	 * @param int $cutomer_id
	 * @return null
	 */
	public function processLedgerForCustomer($customer_id){
		$this->refreshRelation('subscriptionDays');
		$subscriptionDays = $this->subscriptionDays;

		$this->clearLedger($customer_id);

		foreach ($subscriptionDays as $subscriptionDay) {
			$this->addLedgerEntry($customer_id, $subscriptionDay->id);
		}
	}

	/**
	 * Adds new ledgers for all Customers of this subscription.
	 * @return null
	 */
	public function processLedgerForAllCustomers(){
		$this->refreshReleation('subscriptionDays');
		foreach($this->customers as $customer){
			$this->processLedgerForCustomer($customer->id);
		}
	}


	/**
	 * @return \yii\db\ActiveRelation
	 */
	/* getItems is defined in the Parent class */
	public function getCustomer() { return $this->hasOne(Customer::className(), array('id' => 'customer_id')); }
	public function getSubscriptionProduct() { return $this->hasOne(SubscriptionProduct::className(), array('subscription_id' => 'id')); }
	public function getCustomerSubscription() { return $this->hasOne(CustomerSubscription::className(), array('subscription_id' => 'id')); }
	public function getSubscriptionDays() { return $this->hasMany(SubscriptionDay::className(), array('subscription_id' => 'id')); }
	public function getSubscriptionLedgers() { return $this->hasMany(SubscriptionLedger::className(), array('subscription_id' => 'id')); }
	public function getCustomers() {
		return $this
			->hasMany('common\models\Customer', array('id' => 'customer_id'))
			->viaTable('customersubscription', array('subscription_id' => 'id'));
	}

	public function search($params) {
		$query = Subscription::find();
			$dataProvider = new ActiveDataProvider(array(
			'query' => $query,
		));

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'name', true);
		$this->addCondition($query, 'public');
		$this->addCondition($query, 'featured');
		$this->addCondition($query, 'customer_id');
		$this->addCondition($query, 'created');
		$this->addCondition($query, 'modified');
		return $dataProvider;
	}

}
