<?php

namespace freshrealm\subscription\base\models;
use common\helpers\Time;

class SubscriptionLedger extends \common\components\ActiveRecord {
	public static function tableName() {
		return 'subscriptionledger';
	}

	public function rules() {
		return array(
			array(['subscription_id', 'customer_id'], 'required'),
			array(['subscription_id', 'customer_id', 'order_id'], 'integer'),
			array(['process_after', 'processed_time'], 'safe')
		);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'subscription_id' => 'Subscription ID',
			'customer_id' => 'Customer ID',
			'process_after' => 'Process After',
			'processed_time' => 'Processed Time',
			'order_id' => 'Order ID',
		);
	}

	public function scopes(){
		return array(
			'default'=>array(
				'order'=>'process_after DESC'
			),
		);
	}

	public function getStatus(){
		return is_null($this->order_id) ? 'Not Ordered' : 'Ordered';
	}

	public function getSoonEnough(){
		$now = Time::localNow("America/Los_Angeles");
		$process_after = $this->process_after;
		return Time::firstDateLater($now, $process_after);
	}

	public function getTooLate(){
		$now = Time::localNow("America/Los_Angeles");
		$cutoff_time = $this->cutoff_time;
		return Time::firstDateLater($now, $cutoff_time);
	}

	public function getOrderable(){
		if($this->order_id) return false;
		if(!$this->soonEnough) return false;
		if($this->tooLate) return false;

		return true;
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getOrder(){
		return $this->hasOne('\common\models\Order', array('id' => 'order_id'));
	}

	public function getSubscription(){
		return $this->hasOne('freshrealm\subscription\base\models\Subscription', array('id' => 'subscription_id'));
	}
	public function getCustomer(){
		return $this->hasOne('\common\models\Customer', array('id' => 'customer_id'));
	}
}
