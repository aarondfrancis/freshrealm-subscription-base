<?php

namespace freshrealm\subscription\base\models;

class CustomerSubscription extends \common\components\ActiveRecord {
	public static function tableName() {
		return 'customersubscription';
	}

	public function rules() {
		return array(
			array(['customer_id', 'subscription_id'], 'integer'),
		);
	}

	public function beforeSave($insert){
		$exists = self::findBySql(
			"SELECT * FROM {{" . self::tableName() . "}} WHERE ([[customer_id]], [[subscription_id]]) = (:customer_id, :subscription_id)",
			array(
				':customer_id'=>$this->customer_id,
				':subscription_id'=>$this->subscription_id,
			)
		)->all();
		if($exists){
			$this->addError('customer_id', 'This customer is already subscribed.');
			return false;
		}
		return parent::beforeSave($insert);
	}

	public function attributeLabels() {
		return array(
			'customer_id' => 'Customer ID',
			'subscription_id' => 'Subscription ID'
		);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getCustomer() { return $this->hasOne(Customer::className(), array('id' => 'customer_id')); }
	public function getSubscription() { return $this->hasOne(Subscription::className(), array('id' => 'subscription_id')); }
}
