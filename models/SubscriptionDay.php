<?php

namespace freshrealm\subscription\base\models;
use common\helpers\Time;

class SubscriptionDay extends \common\components\ActiveRecord {
	public static function tableName() {
		return 'subscriptionday';
	}

	public $days = array(
		1 => "Sunday",
		2 => "Monday",
		3 => "Tuesday",
		4 => "Wednesday",
		5 => "Thursday",
		6 => "Friday",
		7 => "Saturday",
	);

	public $orderWindowHours = 18;

	public function rules() {
		return array(
			array(['id', 'subscription_id', 'deliver_day', 'order_cutoff_day'], 'integer'),
			array('order_cutoff_time', 'safe')
		);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'subscription_id' => 'Subscription ID',
			'deliver_day' => 'Deliver Day',
			'order_cutoff_day' => 'Order Cutoff Day',
			'order_cutoff_time' => 'Order Cutoff Time',
		);
	}
	/**
	 * Finds the next calendar date and time that matches the day of the week
	 * for this SubscriptionDay.
	 * @return DateTime object
	 */
	public function nextOrderCutoff(){
		$tz = 'America/Los_Angeles';
		for($i=1; $i<=7; $i++){
			$offset = Time::localWithOffset("+$i days", $tz, false);

			// get the day of the week as a 1-7, matches $days as defined in
			// SubscriptionDay model.
			$dayOfTheWeek = $offset->format('w')+1;

			if($dayOfTheWeek == $this->order_cutoff_day){
				$cutoff = $offset->format('Y-m-d ') . $this->order_cutoff_time;
				$cutoff = new \DateTime($cutoff, new \DateTimeZone($tz));
				return $cutoff;
			}
		}
	}

	/**
	 * Returns when the order window opens. Based on the order cutoff and the
	 * order window hours.
	 *
	 * @return string date + time, ready to insert into the DB.
	 */
	public function nextOrderWindowStart(){
		$cutoff = $this->nextOrderCutoff();
		return date(Time::DB_FORMAT, strtotime($cutoff->format(Time::DB_FORMAT) . " -{$this->orderWindowHours} hours"));
	}

	public function getSubscription() { return $this->hasOne('Subscription', array('id' => 'subscription_id')); }

}
