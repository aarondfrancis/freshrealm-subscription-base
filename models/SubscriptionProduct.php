<?php

namespace freshrealm\subscription\base\models;

class SubscriptionProduct extends \common\models\BasketCollection {

	/**
	 * See BasketCollection::basketClass
	 * @return string
	 */
	public function basketClass(){
		return "freshrealm\subscription\base\models\Subscription";
	}

	/**
	 * See BasketCollection::itemClass
	 * @return string
	 */
	public function itemClass(){
		return "common\models\Product";
	}

	/**
	 * Table name for this linker class
	 */
	public static function tableName() {
		return 'subscriptionproduct';
	}

	/**
	 * This is Yii's standard rules() function, but renamed
	 * so it will automatically include the parent's rules.
	 *
	 * @return array
	 */
	public function extraRules() {}
	public function attributeLabels() {}

	/**
	 * Aliases
	 */
	public function getSubscription(){
		return $this->getBasket();
	}

	public function getProduct(){
		return $this->getItem();
	}
}
