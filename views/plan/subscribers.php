<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Product;
use common\models\Customer;

$this->params['breadcrumbs'][] = array('label' => 'Subscriptions', 'url' => array('index'));
$this->params['breadcrumbs'][] = array('label' => $model->name, 'url'=>array('subscription', 'id'=>$model->id));
$this->params['breadcrumbs'][] = 'Subscribers';


?>
<?php echo $this->context->renderPartial('_header', array('model'=>$model)); ?>
<?php if($this->context->can('edit')): ?>
	<h4>Add a Subscriber</h4>
	<?php $form = ActiveForm::begin(); ?>
		<table class='table table-bordered table-striped'>
			<tr>
				<td>
					<?php echo $form->field($customerSubscription, 'customer_id',['template'=>"{input}\n{error}"])->dropDownList(Customer::getForDropDown()); ?>
				</td>
				<td style='width:10%'><button type='submit' class='btn btn-primary btn-small'>Add</button></td>
			</tr>
		</table>
	<?php ActiveForm::end(); ?>
<?php endif; ?>


<?php
	$provider = new ActiveDataProvider(array(
		'query' => \common\models\CustomerSubscription::find()
			->where('subscription_id=:id',array('id'=>$model->id)),
		'pagination' => array(
			'pageSize' => 20,
		)
	));
	echo Html::beginForm();
	echo Gridview::widget(array(
		'dataProvider' => $provider,
		'layout'=>'{items}',
		'columns'=>[
			[
				'class' => 'yii\grid\SerialColumn',
				'options' => [
					'width' => '3%',
				],
			],
			[
				'attribute' => 'customer_id',
				'label' => 'Customer ID',
				'options' => [
					'width' => '10%',
				],
			],
			[
				'label' => 'Customer Name',
				'value' => function ($data) {
					return $data['customer']->full_name;
				},
			],
			[
				'label' => ' ',
				'value' => function($data){
					return "<button type='submit' class='btn btn-danger btn-xs' name='removeCustomer' value='{$data->customer_id}'>Unsubscribe</button>";
				},
				'format' => 'raw',
				'options' => [
					'width' => '10%',
				],
				'visible'=>$this->context->can('edit')
			]
		]
	));
	echo Html::endForm();
?>