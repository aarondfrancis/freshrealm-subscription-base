<?php

use yii\helpers\Html;

/**
 * @var yii\base\View $this
 * @var common\models\Subscription $model
 */

$this->title = 'Create Subscription';
$this->params['breadcrumbs'][] = array('label' => 'Subscriptions', 'url' => array('index'));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-create">
    <h1><?php echo Html::encode($this->title); ?></h1>

    <?php echo $this->render('_form', array(
        'model' => $model
    )); ?>

</div>