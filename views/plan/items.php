<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Product;

/**
 * @var yii\base\View $this
 * @var common\models\Subscription $model
 */


$this->title = $model->name . ' Items';
$this->params['breadcrumbs'][] = array('label' => 'Subscriptions', 'url' => array('index'));
$this->params['breadcrumbs'][] = array('label' => $model->name, 'url'=>array('subscription', 'id'=>$model->id));
$this->params['breadcrumbs'][] = 'Items';

?>
<div class="subscription-view">
	<?php echo $this->context->renderPartial('_header', array('model'=>$model)); ?>
	<?php if($this->context->can('edit')): ?>
		<h4>Add an Item</h4>
		<?php $form = ActiveForm::begin(); ?>
			<table class='table table-bordered table-striped'>
				<tr>
					<td>
						<?php echo $form->field($subscriptionProduct, 'product_id',['template'=>'{input}'])->dropDownList(Product::getForDropDown()); ?>
					</td>
					<td style='width:9%'>
						<?php echo $form->field($subscriptionProduct, 'quantity',['template'=>'{input}'])->textInput(); ?>
					</td>
					<td style='width:10%'><button type='submit' class='btn btn-primary btn-small'>Add</button></td>
				</tr>
			</table>
		<?php ActiveForm::end(); ?>
		<hr>
	<?php endif; ?>
	<?php
		$provider = new ActiveDataProvider(array(
			'query' => \common\models\SubscriptionProduct::find()->where('subscription_id=:id',array('id'=>$model->id)),
			'pagination' => array(
				'pageSize' => 20,
			),
		));
		echo Html::beginForm();
		echo Gridview::widget(array(
			'dataProvider' => $provider,
			'layout'=>'{items}',
			'columns'=>[
				[
					'class' => 'yii\grid\SerialColumn',
					'options' => [
						'width' => '3%',
					],
				],
				[
					'attribute' => 'product_id',
					'label' => 'Product ID',
					'options' => [
						'width' => '8%',
					],
				],
				[
					'label' => 'Product Name',
					'value' => function ($data) {
						return $data['product']->fresh_name;
					},
				],
				[
					'label' => 'Qty',
					'attribute' => 'quantity',
					'headerOptions' => [
						'class' => 'sort-numeric'
					],
					'options' => [
						'width' => '9%',
					],
				],
				[
					'label' => 'Actions',
					'value' => function($data){
						return "<button type='submit' class='btn btn-danger btn-xs' name='removeProduct' value='{$data->product_id}'>Remove</button>";
					},
					'format' => 'raw',
					'options' => [
						'width' => '10%',
					],
					'visible'=>$this->context->can('edit')
				]
			]
		));
		echo Html::endForm();
	?>
</div>