<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Product;

/**
 * @var yii\base\View $this
 * @var common\models\Subscription $model
 */


$this->title = $model->name . ' Orders';
$this->params['breadcrumbs'][] = array('label' => 'Subscriptions', 'url' => array('index'));
$this->params['breadcrumbs'][] = array('label' => $model->name, 'url'=>array('subscription', 'id'=>$model->id));
$this->params['breadcrumbs'][] = 'Orders';

?>
<div class="subscription-view">
	<?php echo $this->context->renderPartial('_header', array('model'=>$model)); ?>
	<?php
		$provider = new ActiveDataProvider(array(
			'query' => \common\models\SubscriptionLedger::find()
				->where('subscription_id=:id',array('id'=>$model->id)),
			'pagination' => array(
				'pageSize' => 20,
			),
		));
		echo Html::beginForm();
		echo Gridview::widget(array(
			'dataProvider' => $provider,
			'layout'=>'{items}',
			'columns'=>[
				[
					'class' => 'yii\grid\SerialColumn',
					'options' => [
						'width' => '3%',
					],
				],
				[
					'label' => 'Customer',
					'value' => function ($data) {
						return $data['customer']->full_name;
					},
					'options' => [
						'width' => '8%',
					],
				],
				[
					'label' => 'Available to Process',
					'value' => function ($data) {
						return $data['process_after'];
					},
					'options' => [
						'width' => '8%',
					],
				],
				[
					'label' => 'Status',
					'value' => function ($data) {
						return $data['status'];
					},
					'options' => [
						'width' => '8%',
					],
				],
				[
					'label' => 'Actions',
					'value' => function ($data) {
						if($data['orderable']){
							return "<a href='" . Yii::$app->getUrlManager()->createUrl('ledger/order', array('id'=>$data['id'])) . "' class='btn btn-xs btn-primary' value='" . $data['id'] . "'>Submit Order</a>";
						}else if($data['status'] == 'Ordered'){
							return "<submit class='btn btn-xs btn-default'>View Order</submit>";
						}else{
							return "";
						}
					},
					'format'=>'raw',
					'options' => [
						'width' => '8%',
					],
					'visible'=>$this->context->can('edit')
				]
			]
		));
		echo Html::endForm();
	?>
</div>