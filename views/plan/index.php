<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\base\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\Subscription $searchModel
 */

$this->title = 'Subscriptions';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="subscription-index">

	<h1><?php echo Html::encode($this->title); ?></h1>
	<p>
		<?php
			if($this->context->can('create')){
				echo Html::a('New Subscription', array('create'), array('class' => 'btn btn-primary'));
			}
		?>
	</p>

	<?php echo GridView::widget(array(
		'dataProvider' => $dataProvider,
		'layout'=>'{items}',
		'columns'=>[
			['attribute'=>'id'],
			['attribute'=>'name'],
			[
				'attribute'=>'public',
				'format'=>'boolean'
			],
			[
				'label'=>'Subscribers',
				'value'=>function($data){
					return count($data->customers);
				}
			],
			[
				'label'=>'Items',
				'value'=>function($data){
					return $data->totalQuantity;
				}
			],
			'cost','price',
			[
				'label' => ' ',
				'value' => function($data){
					return "<a class='btn btn-primary btn-xs' href='" .
					Yii::$app->getUrlManager()->createUrl(array('plan/view','id'=>1)) . "'>View</a>";
				},
				'format' => 'html',
				'options' => [
					'width' => '20%',
				],
			]
		]
	)); ?>

</div>