<?php

use yii\helpers\Html;

/**
 * @var yii\base\View $this
 * @var common\models\Subscription $model
 */

$this->title = $model->name . ' Items';
$this->params['breadcrumbs'][] = array('label' => 'Subscriptions', 'url' => array('index'));
$this->params['breadcrumbs'][] = array('label' => $model->name, 'url'=>array('subscription', 'id'=>$model->id));
$this->params['breadcrumbs'][] = 'Update';

?>
<?php echo $this->context->renderPartial('_header', array('model'=>$model)); ?>
<?php echo $this->render('_form', array(
	'model' => $model,
)); ?>
