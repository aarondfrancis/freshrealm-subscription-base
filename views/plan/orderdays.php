<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Tabs;
use common\models\SubscriptionProduct;


/**
 * @var yii\base\View $this
 * @var common\models\Subscription $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = array('label' => 'Subscriptions', 'url' => array('index'));
$this->params['breadcrumbs'][] = array('label' => $model->name, 'url'=>array('subscription', 'id'=>$model->id));
$this->params['breadcrumbs'][] = 'Order Days';

?>
<?php echo $this->context->renderPartial('_header', array('model'=>$model)); ?>
<?php if($this->context->can('edit')): ?>
	<h4>Add a New Order Day</h4>
	<div class='row'>
		<div class='col-md-3'>
			<strong>Delivery Day</strong>
		</div>
		<div class='col-md-3'>
			<strong>Order Day</strong>
		</div>
		<div class='col-md-3'>
			<strong>Order Time</strong>
		</div>
		<div class='col-md-3'>

		</div>
	</div>
	<?php
		echo $this->context->renderPartial('/subscription-day/_form', array('model'=>$newSubscriptionDay));
	?>
<?php endif; ?>
<?php
	$provider = new ActiveDataProvider(array(
		'query' => \common\models\SubscriptionDay::find()
			->where('subscription_id=:id',array('id'=>$model->id)),
		'pagination' => array(
			'pageSize' => 100,
		)
	));
	echo Html::beginForm();
	echo Gridview::widget(array(
		'dataProvider' => $provider,
		'layout'=>'{items}',
		'columns'=>[
			[
				'class' => 'yii\grid\SerialColumn',
				'options' => [
					'width' => '3%',
				],
			],
			[
				'label' => 'Delivery Day',
				'options' => [
					'width' => '10%',
				],
				'value' => function ($data) {
					return $data['days'][$data['deliver_day']];
				},
			],
			[
				'label' => 'Order Cutoff Day',
				'value' => function ($data) {
					return $data['days'][$data['order_cutoff_day']];
				},
			],
			[
				'label' => 'Order Cutoff Time',
				'value' => function ($data) {
					return $data['order_cutoff_time'] . " (PST)";
				},
			],
			[
				'label' => ' ',
				'value' => function($data){
					return "<button type='submit' class='btn btn-danger btn-xs' name='removeOrderDay' value='{$data->id}'>Remove</button>";
				},
				'format' => 'raw',
				'options' => [
					'width' => '10%',
				],
				'visible' => $this->context->can('edit')
			]
		]
	));
	echo Html::endForm();
?>