<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Tabs;
use common\models\SubscriptionProduct;


/**
 * @var yii\base\View $this
 * @var common\models\Subscription $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = array('label' => 'Subscriptions', 'url' => array('index'));
$this->params['breadcrumbs'][] = $this->title;
?>
<?php echo $this->context->renderPartial('_header', array('model'=>$model)); ?>
<?php echo DetailView::widget(array(
	'model' => $model,
	'attributes' => array(
		'id',
		'name',
		'public:boolean',
		'featured:boolean',
		'customer_id',
		'created',
		'modified',
		'price',
		'cost',
	),
)); ?>
