<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\base\View $this
 * @var common\models\Subscription $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="subscription-form">
	<?php $form = ActiveForm::begin(); ?>
		<div class='row'>
			<div class='col-md-12'>
				<?php echo $form->field($model, 'name')->textInput(array('maxlength' => 255)); ?>
				<?php echo $form->field($model, 'price')->textInput(array(

				))->hint("<small><strong>Note:</strong> The raw cost of all the items in this
				subscription is \${$model->cost}, so price accordingly.</small>"); ?>
				<?php echo $form->field($model, 'public')->checkbox(); ?>
				<?php echo $form->field($model, 'featured')->checkbox(); ?>
			</div>
		</div>


		<div class="form-group">
			<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', array('class' => 'btn btn-primary')); ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
