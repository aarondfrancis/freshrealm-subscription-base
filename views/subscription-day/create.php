<?php

use yii\helpers\Html;

/**
 * @var yii\base\View $this
 * @var common\models\SubscriptionDay $model
 */

$this->title = 'Create Subscription Day';
$this->params['breadcrumbs'][] = array('label' => 'Subscription Days', 'url' => array('index'));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-day-create">

	<h1><?php echo Html::encode($this->title); ?></h1>

	<?php echo $this->render('_form', array(
		'model' => $model,
	)); ?>

</div>
