<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\SubscriptionDay;

/**
 * @var yii\base\View $this
 * @var common\models\SubscriptionDay $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class='row'>
	<?php $form = ActiveForm::begin(); ?>
		<?php echo Html::activeHiddenInput($model, 'id'); ?>
		<div class='col-md-3'>
			<?php echo $form
				->field(
					$model,
					'deliver_day',
					array('template'=>"{input}\n{error}"))
				->dropDownList($model->days); ?>
		</div>
		<div class='col-md-3'>
			<?php echo $form
				->field(
					$model,
					'order_cutoff_day',
					array('template'=>"{input}\n{error}"))
				->dropDownList($model->days); ?>
		</div>
		<div class='col-md-3'>
			<?php echo $form->field($model, 'order_cutoff_time', array('template'=>"{input}\n{error}"))->textInput(); ?>
		</div>
		<div class='col-md-3'>
			<?php if($model->isNewRecord){
				echo Html::submitButton('Create', array('class' => 'btn btn-primary'));
			}else{
				echo Html::submitButton('Remove', array('class' => 'btn btn-danger'));
			} ?>

		</div>
	<?php ActiveForm::end(); ?>
</div>