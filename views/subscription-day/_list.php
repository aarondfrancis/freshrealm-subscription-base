<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\SubscriptionDay;

/**
 * @var yii\base\View $this
 * @var common\models\SubscriptionDay $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class='row'>
	<?php $form = ActiveForm::begin(); ?>
		<?php echo Html::activeHiddenInput($model, 'id'); ?>
		<div class='col-md-3'>
			<?php echo $model->days[$model->deliver_day]; ?>
		</div>
		<div class='col-md-3'>
			<?php echo $model->days[$model->order_cutoff_day]; ?>
		</div>
		<div class='col-md-3'>
			<?php echo $model->order_cutoff_time; ?> (PST)
		</div>
		<div class='col-md-3'>
			<?php
				echo Html::submitButton('Remove', array('class' => 'btn btn-xs btn-danger'));
			?>

		</div>
	<?php ActiveForm::end(); ?>
</div>
<br />