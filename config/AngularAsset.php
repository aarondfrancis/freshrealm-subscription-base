<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace freshrealm\subscription\base\config;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AngularAsset extends AssetBundle
{
	public function init() {
        parent::init();
        $this->sourcePath = dirname(__DIR__) . '/web';
    }

	public $js = array(
		'app/subscription.js',
	);
}
