<?php

use yii\db\Schema;

class m140321_163111_first extends \yii\db\Migration {
    public function up() {
		$sql = file_get_contents(__DIR__ . "/first.sql");
		$this->execute($sql);
    }

    public function down() {
    	$this->dropTable('subscriptionproduct');
    	$this->dropTable('subscriptionledger');
    	$this->dropTable('subscriptionday');
    	$this->dropTable('customersubscription');
    	$this->dropTable('subscription');
    }
}
