<?php

namespace freshrealm\subscription\base;

use Yii;

/**
 * SubscriptionBase module
 *
 * @author Aaron Francis <aarondfrancis@gmail.com>
 */
class Module extends \yii\base\Module {
	public $defaultRoute = 'default';

	public function migrationPath(){
		return __DIR__ . "/migrations";
	}

	public function postInstall(){
		echo "SubscriptionBase installed.";
	}

	public function postUninstall(){
		echo "SubscriptionBase uninstalled.";
	}


	/**
	 * Returns an array full of UrlRules, just like in the main
	 * config file. The only difference is that {module} will be
	 * replaced with the uniqueId of this module.
	 */
	public $urlRules = [
		array(
			'pattern' => '{module}/<controller:\w+>/<id:\d+>/<action:\w+>',
			'route' => '{module}/<controller>/<action>',
			'defaults' => array(
				'action' => 'view'
			)
		)
	];

	/**
	 * RBAC rules. The key is the name and will be prefixed
	 * with the uniqueId of this module. You can add/remove
	 * items/tasks or add/remove children and the user assignments
	 * of the unaffected tasks/roles will remain the same.
	 */
	public $rbac = [
		'view-items' => [
			'description'=>'View items in a subscription',
			'type'=>'operation'
		],
		'view' => [
			'description'=>'View a single subscription',
			'type'=>'operation',
			'children'=>['view-items']
		],
		'create' => [
			'description'=>'Create a new subscription',
			'type'=>'task'
		],
		'list' => [
			'description'=>'View all subscriptions',
			'type'=>'task',
			'children'=>['view']
		],
		'edit' => [
			'description'=>'Edits subscriptions',
			'type'=>'task',
			'children'=>['view']
		],
		'delete' => [
			'description'=>'Delete an existing subscription',
			'type'=>'task',
			'children'=>['view']
		]
	];
}