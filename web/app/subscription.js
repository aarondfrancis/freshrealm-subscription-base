'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
var frSubscription = angular.module('frSubscription', []).
    value('version', '0.1').
    directive('appVersion', ['version', function(version) {
        return function(scope, elm, attrs) {
            elm.text(version);
        };
    }]);
